import express from 'express';
import cors from 'cors';
import serverless from 'serverless-http';

let app = express(); 

app.use(cors());

let port = process.env.PORT || 5000;
const router = express.Router(); 

router.get('/test', (req, res) => {
    res.json({'message': 'hola mundo'});
});

app.use('/.netlify/functions/api', router);

export const handler = serverless(app);

